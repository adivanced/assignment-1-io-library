
section .text

 
; Принимает код возврата и завершает текущий процесс
exit: ; di - excode
    mov rax, 60
    syscall
ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length: ; di - str ptr
    cld
    xor al, al
    mov rcx, 0xffffffffffffffff ; NOT a magic value. We just use repne scasb, wich asks for length in rcx. Since we work with 0-terminated strings, we put the biggest number, we can fit into the length, since the length is determined via the 0-terminator.
    repne scasb
    mov rax, 0xfffffffffffffffe ; Here we just calculate the strings length, wich is the value we initially put in rcx (lets call it A) minus the value we got in rcx after the repne scasb(lets call it B), minus one. So length = A-B-1 (that is why we have 0xfffffffffffffffe - it is equal to A - 1). I hope I explained it well and no one will ever bluntly call that pretty straightforward calculation as a "Magic Constants" again.
    sub rax, rcx
ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string: ; di = str ptr
    push rdi
    call string_length
    pop rsi
    mov rdx, rax

    mov rax, 1
    mov rdi, 1

    syscall

ret

; Принимает код символа и выводит его в stdout
print_char: ; di = char
    dec rsp
    mov byte [rsp], dil

    mov rax, 1
    mov rdi, 1
    mov rsi, rsp  
    mov rdx, 1
    syscall

    inc rsp

ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, 10
jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: ; di - uint
    mov rax, rdi
    xor rsi, rsi
    mov rcx, 10
    ._loop:
        xor rdx, rdx
        div rcx
        add dl, '0'
        dec rsp
        mov byte [rsp], dl
        inc rsi
        test rax, rax
        jnz ._loop

    mov rax, 1
    mov rdi, 1
    mov rdx, rsi
    mov rsi, rsp
    syscall

    add rsp, rdx

ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int: ; di - number
    cmp rdi, 0
    jl ._pminus
jmp print_uint

    ._pminus:
        mov r8, rdi
        mov dil, '-'
        call print_char
        mov rdi, r8
        neg rdi
jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ._loop:
        cmp byte [rdi], 0
        jz ._f_end_1
        cmp byte [rsi], 0
        jz ._f_end_2
        cmpsb
        jnz ._not_equal
    jmp ._loop
    ._not_equal:
        xor rax, rax
    ret
    ._f_end_1:
        cmp byte [rsi], 0
        jnz ._not_equal
        mov rax, 1
    ret
    ._f_end_2:
        cmp byte [rdi], 0
        jnz ._not_equal
        mov rax, 1
    ret ; affects: di, si, ax
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    or ah, al
    mov al, byte [rsp]
    inc rsp
ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    lea r10, [rsi-1] ; consider the 0 in the end of the buffer
    mov r8, rdi
    xor r9, r9  ; counter
    ._loop:
        call read_char
        or ah, ah
        jz ._end1
        cmp al, 0x20
        je ._loop
        cmp al, 0x9
        je ._loop
        cmp al, 0xa
        je ._loop

        mov byte [r8+r9], al
        inc r9

    ._r_loop:
        call read_char
        or ah, ah
        jz ._end2
        cmp al, 0x20
        je ._end2
        cmp al, 0x9
        je ._end2
        cmp al, 0xa
        je ._end2
        mov byte [r8+r9], al
        inc r9
        cmp r9, r10
        jae ._end1
    jmp ._r_loop

    ._end1:
        xor rdx, rdx
        xor rax, rax
    ret
    ._end2:
        mov rax, r8
        mov rdx, r9
        mov byte [rax+rdx], 0
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx ; len
    ._searchloop:
        cmp byte [rdi+rcx], '0'
        jae ._upper_check
    ._e_sloop:
        or rcx, rcx
        jz ._end
        mov rsi, 1  ; multiplyer
        xor rdx, rdx
        xor r9, r9  ; number
        mov r10, rcx
    ._calcloop:
        xor rax, rax
        mov al, byte [rdi+rcx-1]
        sub al, '0'
        mul rsi
        add r9, rax
        lea rsi, [rsi*4+rsi]
        shl rsi, 1
        dec rcx
        jnz ._calcloop
    mov rax, r9
    mov rdx, r10
ret
    ._upper_check:
        cmp byte [rdi+rcx], '9'
        ja ._e_sloop
        inc rcx
    jmp ._searchloop

    ._end:
        xor rax, rax
        xor rdx, rdx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int: ; di - str
    cmp byte [rdi], '-'
    jz ._fminus
jmp parse_uint
    ._fminus:
        inc rdi
        call parse_uint
        or rdx, rdx
        jz ._end
        inc rdx
        neg rax
ret
._end:
ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: ; di - str ptr, si - buf ptr, dx - buf len
    mov r8, rdi
    call string_length
    mov rdi, r8
    inc rax

    cmp rdx, rax
    jae ._buf_bigger

    xor rax, rax
ret

    jmp ._end

    ._buf_bigger:
        mov rcx, rsi
        mov rsi, rdi ;xchg rdi, rsi
        mov rdi, rcx

        mov rcx, rax
        rep movsb

._end:

ret
